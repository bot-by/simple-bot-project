# Simple Bot Project

This archetype creates a single module Maven project with [jUnit][], [Hamcrest][], [Mockito][] and [slf4j][] dependencies.
New project contains a base [GitLab CI/CD config][CI] and run [JaCoCo][] and [Codacy][] checks on build.

[![pipeline status](https://gitlab.com/bot-by/simple-bot-project/badges/master/pipeline.svg)](https://gitlab.com/bot-by/simple-bot-project/commits/master)

## Usage

1. Create new project `feature-artifact` on GitLab.
1. Go to **Settings** >> **General** >> **Visibility** and turn off **CI/CD**. Do not forget to save changes.
1. Create new project `feature-artifact` locally
```shell
mvn archetype:generate -DinteractiveMode=false \
    -DgroupId=com.example \
    -DartifactId=feature-artifact \
    -Dpackage=com.example.feature \
    -DlambdaName=my3words-bot \
    -DfullName=Feature \
    -DlongDescription=Your\ feature\'s\ description. \
    -DinceptionYear=2021 \
    -DownerName=John\ Smith \
    -DprojectHomepage=http://example.com/projects/feature \
    -Dgoals=verify \
    -DarchetypeGroupId=uk.bot-by -DarchetypeArtifactId=simple-bot-project
```
   It creates new folder `feature-artifact` then run tests.
1. Go to the folder `feature-artifact` and prepare Git repository
```shell
git init
git add .gitignore
git config user.name "John Smith"
git config user.email "John_Smith@tutanota.com"
git config pull.ff only
git remote add origin git@gitlab.com:john_smith/feature-artifact.git
git config --local --list
cat > .gitignore <<EOF
/target/
/.idea/
/.flattened-pom.xml

*.iml
*~
EOF
```
1. Push project to GitLab `git push --set-upstream origin master`
1. Add GitLab repository to Codacy.
1. Enable **CI/CD** on GitLab, get a project token from Codacy and set as environment variable `CODACY_PROJECT_TOKEN` on GitLab.
1. Run pipeline.

## History

See [ChangeLog](changelog.md)

## License

[Apache License v2.0](LICENSE)
[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat)](http://www.apache.org/licenses/LICENSE-2.0.html)

[jUnit]: https://junit.org/junit5/
[Hamcrest]: http://hamcrest.org/JavaHamcrest/
[Mockito]: https://site.mockito.org/
[slf4j]: http://www.slf4j.org/
[CI]: https://docs.gitlab.com/ee/ci/yaml/
[JaCoCo]: https://www.eclemma.org/jacoco/
[Codacy]: https://www.codacy.com/

