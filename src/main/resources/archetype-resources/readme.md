#set( $symbol_hash = '#' )
${symbol_hash} ${fullName}

TODO: Write a project description

${symbol_hash}${symbol_hash} Installation

TODO: Describe the installation process

${symbol_hash}${symbol_hash} Usage

TODO: Write usage instructions

${symbol_hash}${symbol_hash} Contributing

Please read [Contributing](contributing.md) for details on our code of conduct, and the process for submitting pull requests to us.

${symbol_hash}${symbol_hash} History

See [ChangeLog](changelog.md)

${symbol_hash}${symbol_hash} Credits

TODO: Write credits

${symbol_hash}${symbol_hash} License

[Apache License v2.0](LICENSE)
[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat)](http://www.apache.org/licenses/LICENSE-2.0.html)
