#set( $symbol_hash = '#' )
${symbol_hash} Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

${symbol_hash}${symbol_hash} Unreleased

${symbol_hash}${symbol_hash} ${version} - ${inceptionYear}-XX-XX
${symbol_hash}${symbol_hash}${symbol_hash} Added
-   Initial release.
