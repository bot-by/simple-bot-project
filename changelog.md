# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## 1.0.5 - 2021-07-08
### Updated
-   Update versions of dependencies and plugins

## 1.0.2 - 2021-06-02
### Fixed
-   Small mistyping

## 1.0.2 - 2021-06-01
###
-   Added LICENSE
### Updated
-   Dependencies in template
